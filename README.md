
# Terraform module to setup a service on ECS

This module sets up a full customized ECS Service and Task Definition on a given ECS Cluster name.
Please read the instructions for proper set up the module.

## Environment

### Install Terraform  
Check on how to install Terraform on your given OS at [this link](terraform.io/downloads.html)

### Pre-commit tools
This project contains a pre-commit file to execute checks at this source code. Check [Anton Babenko plugin](https://github.com/antonbabenko/pre-commit-terraform) to
install the required dependencies.

## How to

### Deploy this module on your terraform project

In yout terraform project initialize this module like this:

```hcl
module "my-app-service" {
    source = "git@gitlab.com:eudevops/ecs-service.git?ref=main"
    app_name                         = ""
    app_port                         = 80
    auto_scaling_cooldown_time       = 300
    auto_scaling_cpu_threshold_up    = 70
    auto_scaling_memory_threshold_up = 70
    cluster_name                     = ""
    desired_task_cpu                 = 1024
    desired_task_memory              = 2048
    environment_vars                 = []
    filter_subnets_by_tags           = []
    hc_route                         = ""
    hc_status_code                   = ""
    iam_policies                     = []
    iam_policies_for_execution       = []
    image_repository                 = ""
    image_tag                        = "latest"
    is_https                         = ""
    launch_type                      = "EC2"
    load_balancer_name               = ""
    max_tasks                        = ""
    min_tasks                        = 1
    redirect_rules                   = { "host_headers": [], "path_patterns": [] }
    secrets                          = []
    vpc_id                           = ""
}
```

Fill the relevant configuration, note that some variables have a default value and you may not need to declare them above.

> Note: To tag the resources configure the `default_tags` map on aws provider. [Docs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs#default_tags-configuration-block).

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.61.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.61.0 |
| <a name="provider_template"></a> [template](#provider\_template) | 2.2.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_appautoscaling_policy.cpu](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/appautoscaling_policy) | resource |
| [aws_appautoscaling_policy.memory](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/appautoscaling_policy) | resource |
| [aws_appautoscaling_target.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/appautoscaling_target) | resource |
| [aws_cloudwatch_log_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_ecs_service.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_service) | resource |
| [aws_ecs_task_definition.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_task_definition) | resource |
| [aws_iam_role.task](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.exec](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.permissions](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_lb_listener_rule.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_listener_rule) | resource |
| [aws_lb_target_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_target_group) | resource |
| [aws_security_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group_rule.load_balancer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_ecs_cluster.selected](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ecs_cluster) | data source |
| [aws_iam_policy_document.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_lb.selected](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/lb) | data source |
| [aws_lb_listener.selected443](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/lb_listener) | data source |
| [aws_lb_listener.selected80](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/lb_listener) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |
| [aws_security_group.selected](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/security_group) | data source |
| [aws_subnet_ids.subnets](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet_ids) | data source |
| [aws_vpc.selected](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc) | data source |
| [template_file.this](https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_app_name"></a> [app\_name](#input\_app\_name) | Application Name | `string` | n/a | yes |
| <a name="input_app_port"></a> [app\_port](#input\_app\_port) | Port that the application should expose | `number` | `80` | no |
| <a name="input_auto_scaling_cooldown_time"></a> [auto\_scaling\_cooldown\_time](#input\_auto\_scaling\_cooldown\_time) | Cooldown time in ms to wait for scaling up or down by cpu and memory | `number` | `300` | no |
| <a name="input_auto_scaling_cpu_threshold_up"></a> [auto\_scaling\_cpu\_threshold\_up](#input\_auto\_scaling\_cpu\_threshold\_up) | Amount in % of cpu upper threshold | `number` | `70` | no |
| <a name="input_auto_scaling_memory_threshold_up"></a> [auto\_scaling\_memory\_threshold\_up](#input\_auto\_scaling\_memory\_threshold\_up) | Amount in % of memory upper threshold | `number` | `70` | no |
| <a name="input_cluster_name"></a> [cluster\_name](#input\_cluster\_name) | Name of the ECS cluster to deploy | `string` | n/a | yes |
| <a name="input_desired_task_cpu"></a> [desired\_task\_cpu](#input\_desired\_task\_cpu) | Task CPU | `number` | `1024` | no |
| <a name="input_desired_task_memory"></a> [desired\_task\_memory](#input\_desired\_task\_memory) | Task Memory | `number` | `2048` | no |
| <a name="input_environment_vars"></a> [environment\_vars](#input\_environment\_vars) | List of static environment vars, composed by env name and value | <pre>list(object({<br>    name  = string<br>    value = string<br>  }))</pre> | `[]` | no |
| <a name="input_filter_subnets_by_tags"></a> [filter\_subnets\_by\_tags](#input\_filter\_subnets\_by\_tags) | Filter subnets to deploy resources using tags | <pre>list(object({<br>    key    = string<br>    values = list(string)<br>  }))</pre> | `[]` | no |
| <a name="input_hc_route"></a> [hc\_route](#input\_hc\_route) | Route/Path to target for health check | `string` | `null` | no |
| <a name="input_hc_status_code"></a> [hc\_status\_code](#input\_hc\_status\_code) | Health check status code | `number` | `null` | no |
| <a name="input_iam_policies"></a> [iam\_policies](#input\_iam\_policies) | List of IAM policies arns to access aws resources from within the task | `list(string)` | `[]` | no |
| <a name="input_iam_policies_for_execution"></a> [iam\_policies\_for\_execution](#input\_iam\_policies\_for\_execution) | List of IAM policies arns to access aws resources to start the task like SSM, KMS or Secrets Manager | `list(string)` | `[]` | no |
| <a name="input_image_repository"></a> [image\_repository](#input\_image\_repository) | Repository of the image | `string` | n/a | yes |
| <a name="input_image_tag"></a> [image\_tag](#input\_image\_tag) | Image tag to be used on the task definition | `string` | `"latest"` | no |
| <a name="input_is_https"></a> [is\_https](#input\_is\_https) | The load balancer is using https | `bool` | `null` | no |
| <a name="input_launch_type"></a> [launch\_type](#input\_launch\_type) | ECS Service launch type | `string` | `"EC2"` | no |
| <a name="input_load_balancer_name"></a> [load\_balancer\_name](#input\_load\_balancer\_name) | Name of the load balancer to use for deploy | `string` | `""` | no |
| <a name="input_max_tasks"></a> [max\_tasks](#input\_max\_tasks) | Maximum number of running tasks | `number` | n/a | yes |
| <a name="input_min_tasks"></a> [min\_tasks](#input\_min\_tasks) | Minimum number of running tasks | `number` | `1` | no |
| <a name="input_redirect_rules"></a> [redirect\_rules](#input\_redirect\_rules) | Map of redirect rules for load balancer | `map(list(string))` | <pre>{<br>  "host_headers": [],<br>  "path_patterns": []<br>}</pre> | no |
| <a name="input_secrets"></a> [secrets](#input\_secrets) | List of environment vars from aws ssm or kms, composed by env name and secret arn | <pre>list(object({<br>    name      = string<br>    valueFrom = string<br>  }))</pre> | `[]` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | Id of the VPC to deploy the resources | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_iam_role_arn"></a> [iam\_role\_arn](#output\_iam\_role\_arn) | Iam role of the task |
| <a name="output_security_group_id"></a> [security\_group\_id](#output\_security\_group\_id) | Security group id |
| <a name="output_service_id"></a> [service\_id](#output\_service\_id) | ECS Service id |
| <a name="output_task_definition_id"></a> [task\_definition\_id](#output\_task\_definition\_id) | Task definition id |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->